# Revealjs slideshow template

A template for writing slideshows in markdown and transform them into html or pdf.

## Local usage

* Install dependencies :
  * [pandoc](https://pandoc.org/) for html output.
  * [decktape](https://github.com/astefanutti/decktape) for pdf output. 

* Check that you have the proper fonts : [`Poppins semibold`](https://fonts.google.com/specimen/Poppins) & [`Carlito`](https://www.1001fonts.com/carlito-font.html) (or [`Lato`](https://fonts.google.com/specimen/Lato))

* Clone this repository

* Write your presentation in pandoc using [pandoc markdown syntax](https://pandoc.org/MANUAL.html#slide-shows) (default `presentation.md` file shows some examples)

* Convert to a [revealjs](https://revealjs.com/) html presentation with `make render_html` or to a pdf with `make render_pdf` (see Customization section below in case you changed the name of the markdown presentation).

## Gitlab pages builds reveal and pdf presentation

The repository can also be forked on gitlab, and the presentation will be available on gitlab pages.

Both the reveal presentation and the pdf presentation are build and deployed to Gitlab pages with Gitlab CI : [Demo reveal presentation](https://multi-coop.gitlab.io/slider-template) and [demo pdf presentation](https://multi-coop.gitlab.io/slider-template/presentation.pdf). The pdf can be fetched by changing the file extension in the url to `.pdf`.

To render a collection of presentations on the same instance of pages, just repeat `make ci.render_html` and `make ci.render_pdf` with the specific `IN` and optionnaly `OUT` variables in the `.gitlab-ci.yml` file. The first call will determine to which presentation the root will be redirected. 

## Customization

* The name of the presentation and of the output can be changed with environment variables `IN` and `OUT`. `IN` is the markdown source file, and `OUT` is the basename (no extension) of the output files. They can be updated in the header of the "Makefile" accordingly, or passed to the command line as in the following example :
  `make render_html OUT=$(date -I)_presentation`.
* Adding and positionning the logo of the customer on the title page can be done using the yaml header of the markdown file. 
* The default recipe and `all` recipes (`make` & `make all`) can be customized to your liking (`make` defaults to `make help`), see commented block in `Makefile` for an example that renders several presentations at once.

## Screenshots

![Screenshot 1](images/screen1.png)
![Screenshot 2](images/screen2.png)

## Custom classes

To customize your presentation you can use some custom classes predefined in the `style.css` file :

- `.text-micro` (text in gray, small size)
- `.text-nano` (text in gray, smaller size)
- `mark` [tag](https://developer.mozilla.org/fr/docs/Web/HTML/Element/mark) to highlight some parts.

To use custom classes :

- Inline use (span)
```
[Texte]{.class}
```
- Use in a bloc (div)
```
::: {.class}
Texte
:::
```

## Icons

You can use icons in your presentation with the [RemixIcons library](https://remixicon.com).

To include an icon in your markdown :

```md
<i class="ri-download-2-fill"></i>
```

Note : RemixIcons library link is injected in the `heaer_include.html`

```html
<!-- file : ./static/header_include.html -->
<link rel="icon" type="image/x-icon" href="static/favicon.ico"/>
<link href="https://cdn.jsdelivr.net/npm/remixicon@3.5.0/fonts/remixicon.css" rel="stylesheet">
```

## Watermark

If you want to include a watermark image in every slide you can include a css similar to the following snippet :

```css
/* file : ./static/style.css */
body:after {
  content: url(https://mirrors.creativecommons.org/presskit/buttons/88x31/svg/by-sa.svg);
  position: fixed;
  transform: scale(.5);
  top: .5rem;
  right: .5rem;
}
```

This particular snippet displays slides similar to this :

![watermark](images/watermark.png)

## Cards

You can include cards blocks to add a rectangle with a primary or a secondary color. You can also play with the columns to organize your cards on a line.

```md
# Test cards

:::::: {.columns}
:::{.column .text-center .card .card width="50%"}
Card with primary color
:::
:::{.column .text-center .card .card-secondary width="50%"}
Card with secondary color
:::
::::::
```

This block displays a slide similar to this :

![watermark](images/cards.png)

If needed you can customize the cards in the `static/style.css` file.

## Hide a slide

Encapsulate the markdown block with a `data-visibility= "hidden"` block :

```md
::: {data-visibility="hidden"}
## This slide will be skipped

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.

:::
```
