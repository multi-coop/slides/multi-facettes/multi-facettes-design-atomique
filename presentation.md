---
title: Café multi-facettes 🪩 
subtitle: Design Atomique
author: Erica Delagnier
date: 9 avril 2023
title-slide-attributes:
    data-background-image: "static/logo.svg, static/logo_client.svg"
    data-background-size: "auto 10%, auto 12%"
    data-background-position: "95% 95%, 70% 97%"
---

# Concept

Concevoir ses interfaces avec un modéle de réflexion

![](images/figure.png "Illustration d'un atome, molécule, organisme")

source : [Bradfrost.com](https://bradfrost.com/blog/post/atomic-web-design/)

## Une boite à outils 

:::::::::::::: {.columns}
::: {.column width="60%"}
Décomposer une maquette de page en composant

VS 

Constituer page à partir d'une bibliothéque de composants (js + css)
::: 

::: {.column width="40%"}
![](images/decomposition.png)
:::
::::::::::::::

## Principes d'utilisation

:::::::::::::: {.columns}
::: {.column width="60%"}

- Ne jamais créer de composants sans avoir d'abord fait le point de l'existant
- Le cas échéant, créer ou modifier les composants nécessaires
- S'assurer que les principes établis sont consistants à travers toute l'application

::: 

::: {.column width="40%"}
![](images/respect.png)
:::
::::::::::::::

## Contre-exemple 

![](images/eviter.png)


## Composants 1/3

:::::::::::::: {.columns}
::: {.column width="50%"}
**Atomes :**

- Style de texte 
    - titre, sous-titres ...
- Couleurs
    - alerte, navigation ...

![](images/atoms.png "Liste de couleurs")
::: 

::: {.column width="50%"}
**Molécules :**

- Composants associé à un tag HTML 
    - ```<btn>, <input> ...```
![](images/molécules.png "Boutons")
:::
::::::::::::::

## Composants 2/3

:::::::::::::: {.columns}
::: {.column width="50%"}
**Organismes :** 

- Formulaire
- Carte
- Tableau
- Liste

![](images/organism.png "Formulaire")
::: 

::: {.column width="50%"}
**Templates :**

- Structure de page 

![](images/template.png "Ecran de téléphone")
:::
::::::::::::::

## Composants 3/3

Et enfin, Pages complètes: 

![](images/ecran.png "Page d'application")


---

# Mise en pratique

## Maquette Agatha 

![](images/application.png "screenshot de la maquette Figma")

[Figma](https://www.figma.com/file/u1oYAGnXvfJV6SmSyJc0EO/Maquette?type=design&node-id=0%3A1&mode=design&t=IyO6gmtyFhjQprTX-1)

## Code 

:::::: {.columns}
:::{.column .text-center width="50%"}
Cette structure se retrouve dans le code

cf [Github](https://github.com/agatha-budget/front-web/tree/master/src/style/atomic_design)
:::
:::{.column .text-center width="50%"}
![](images/code.png)
:::
::::::


# Des questions ?