{ pkgs ? import <nixpkgs> {} }:

with pkgs;

mkShell {
buildInputs = [
nodejs-18_x pandoc firefox
];
shellHook = ''
        npm install decktape
        export PATH="$PWD/node_modules/.bin/:$PATH"
    '';

}
